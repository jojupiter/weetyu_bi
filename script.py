import json
from pyspark.sql import SparkSession
from pyspark.sql import functions as F

def read_credentials(file_path):
    with open(file_path, 'r') as file:
        credentials = json.load(file)
    return credentials

def copy_data_postgres_to_mongo(credentials):
    postgres_params = credentials['postgres']
    mongo_params = credentials['mongo']

    # Initialisation de la session Spark
    spark = SparkSession.builder \
        .appName("PostgresToMongo") \
        .config("spark.mongodb.input.uri", mongo_params['uri']) \
        .config("spark.mongodb.output.uri", mongo_params['uri']) \
        .getOrCreate()

    # Lecture des données de table1 depuis PostgreSQL
    df_table1 = spark.read \
        .format("jdbc") \
        .option("url", postgres_params['url']) \
        .option("dbtable", "table1") \
        .option("user", postgres_params['user']) \
        .option("password", postgres_params['password']) \
        .load()

    # Copie des données de table1 vers MongoDB
    df_table1.write \
        .format("mongo") \
        .option("database", mongo_params['database']) \
        .option("collection", "table1") \
        .mode("overwrite") \
        .save()

    # Effectuez les mêmes opérations pour table2 et table3

    spark.stop()

if __name__ == "__main__":
    credentials = read_credentials('credentials.json')
    copy_data_postgres_to_mongo(credentials)
