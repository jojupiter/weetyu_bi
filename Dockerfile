# Utiliser une image de PySpark
FROM pyspark/pyspark:3.1.1

# Copier le script Python et le fichier requirements.txt
COPY script.py /app/
COPY requirements.txt /app/

# Définir le répertoire de travail
WORKDIR /app

# Installer les dépendances Python
RUN pip install --no-cache-dir -r requirements.txt

# Exécuter le script Python
CMD ["python", "script.py"]
